﻿Imports EnhancedRandom

' Just wanted to see if I could use my library properly in VB since it was made in C#. '
' Turns out yes, the CLR is magic! '

Module Module1

    Sub Main()
        Console.WriteLine("Esc to quit. Press any key.")

        Dim IsExit As Boolean = False

        While (Not IsExit)
            Dim key = Console.ReadKey()

            If (key.Key = ConsoleKey.Escape) Then
                IsExit = True
            Else
                Console.Clear()

                Dim Rand As New Random

                Dim Strs = New String() {"dog", "cat", "tree", "woof", "meow", "bark (hehe get it)", "sorted"}

                Console.WriteLine("Seed: " + Rand.NextSeed().ToString())
                Console.WriteLine("Lower char: " + Rand.NextLowerChar())
                Console.WriteLine("Upper char: " + Rand.NextUpperChar())
                Console.WriteLine("String: " + Rand.NextString(Strs))

            End If

        End While

    End Sub
End Module
