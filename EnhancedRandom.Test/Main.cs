﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EnhancedRandom;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Esc to quit. Press any key.");

            var isExit = false;

            while (!isExit)
            {
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Escape)
                {
                    isExit = true;
                } else
                {
                    Console.Clear();
                    Next();
                }
            }
        }

        static void Next()
        {
            var rand = new Random();
            var strs = new string[]
            {
                "dog",
                "cat",
                "tree",
                "woof",
                "meow",
                "bark (hehe get it)",
                "sorted",
            };

            Console.WriteLine("Seed: " + rand.NextSeed());
            Console.WriteLine("Lower char: " + rand.NextLowerChar());
            Console.WriteLine("Upper char: " + rand.NextUpperChar());
            Console.WriteLine("String: " + rand.NextString(strs));
        }
    }
}
