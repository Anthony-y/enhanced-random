﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EnhancedRandom
{
    public static class EnhancedRandom
    {
        static readonly string alphabetLower = "abcdefghijklmnopqrstuvwxyz";
        static readonly string alphabetUpper = alphabetLower.ToUpper();

        static readonly int alphabetLength = 26;

        public static char NextLowerChar(this Random rand)
        {
            // 26 letters in the English alphabet, hope you know that :3
            // We don't need to check for out of bounds :P
            // Add one because the upper bound is exclusive.
            return alphabetLower[rand.Next(1, alphabetLength)];
        }

        public static char NextUpperChar(this Random rand)
        {
            // Same for alphabetUpper.
            return alphabetUpper[rand.Next(1, alphabetLength)];
        }

        public static string NextString(this Random rand, string[] strs)
        {
            return strs[rand.Next(1, strs.Length)];
        }

        public static ulong NextSeed(this Random rand)
        {
            try
            {
                ulong seed;

                do
                {
                    // Some weird arbitrary numbers to try and make it nice and random.
                    seed = (ulong)rand.Next(1, 5 + 1) * (ulong)DateTime.Now.Millisecond * 1000 / (ulong)rand.Next(rand.Next(DateTime.Now.Second * 10 / 100), rand.Next(DateTime.Now.Year)) * 10;
                } while (seed < 50);

                return seed;
            } catch
            {
                return rand.NextSeed();
            }
        }
    }
}